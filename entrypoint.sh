#!/bin/bash

apt update && apt install apache2 git -y

deploy_app () {
    cd /guugle && git pull
}

# Si el directorio '/guugle' no existe, clonarlo

if ! test -d /guugle; then

    git clone https://gitlab.com/joacoinfra/guugle.git /guugle

    deploy_app
else

    # Si el repositorio ya se ha cloando
    # traer posibles cambios de la rama main

    deploy_app
fi

exec "$@"
