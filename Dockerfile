FROM debian:latest
COPY /public-html/ /var/www/html/
COPY entrypoint.sh /entrypoint.sh
WORKDIR /var/www/html
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
EXPOSE 80
CMD ["apache2ctl", "-D", "FOREGROUND"]
